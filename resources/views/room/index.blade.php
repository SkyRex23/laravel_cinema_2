@extends('layouts.app')
@section('title', 'All rooms')
@section('content')
<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">All rooms</h1>

        @if (session('ok'))
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        <table class="table table-hover table-striped table-sm">
            <thead>
                <tr>
                    <th scope="row">{{ __('Name') }}</th>
                    <th>{{ __('Capacity') }}</th>
                    <th>{{ __('Cinema') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rooms as $room)
                <tr>
                    <td><a href="/room/{{$room->id}}">{{ $room->name }}</a></td>
                    <td>{{ $room->capacity }}</td>
                    <td>{{ $room->id_cinema }} {{ $room->is_managed_by->name }}</td>

                    <td class="table-action">
                        <a type="button" href="{{ route('room.edit', $room->id) }}" class="btn btn-sm"
                            data-toggle="tooltip" title="@lang('Edit room') {{ $room->name }}">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                        <a type="button" href="{{ route('room.destroy', $room->id) }}"
                            class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip"
                            title="@lang('Delete room') {{$room->name}}">
                            <i class="fas fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="center-element" style="display: flex; justify-content: center;">
            {{ $rooms->appends(request()->except('page'))->links() }}
        </div>
    </div>
</main>
<script>
    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
        })

        $(document).on('click', '.btn-delete', function(){

            let button = $(this);

            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE'
            }).done(function(){
                button.closest('tr').remove();
            });
            return false;
        });
</script>
@endsection