@extends('layouts.app')
@section('title', 'Specific room')
@section('content')

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">{{$room->name}}</h1>

        <ul class="list-group">
            <li class="list-group-item" style="text-align:center;">{{$room->id}}</li>
            <li class="list-group-item" style="text-align:center;">{{$room->name}}</li>
            <li class="list-group-item" style="text-align:center;">{{$room->capacity}}</li>
            <li class="list-group-item" style="text-align:center;">{{$room->cinema_id}}</li>
        </ul>
    </div>
</main>
@endsection