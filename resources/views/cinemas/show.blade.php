@extends('layouts.app')
@section('title', 'Specific cinema')
@section('content')

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">{{ $cinema->name }}</h1>

        <ul class="list-group">
            <li class="list-group-item" style="text-align:center;">{{ $cinema->id }}</li>
            <li class="list-group-item" style="text-align:center;">{{ $cinema->name }}</li>
            <li class="list-group-item" style="text-align:center;">{{ $cinema->street }}</li>
            <li class="list-group-item" style="text-align:center;">{{ $cinema->postcode }}</li>
            <li class="list-group-item" style="text-align:center;">{{ $cinema->country }}</li>
            <li class="list-group-item" style="text-align:center;">{{ $cinema->city }}</li>
        </ul>
    </div>
</main>
@endsection