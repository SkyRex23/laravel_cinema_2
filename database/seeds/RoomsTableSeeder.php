<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([[
            'name' => 'Room I',
            'capacity' => '200',
            'cinema_id' => '1',
        ],[
            'name' => 'Room II',
            'capacity' => '250',
            'cinema_id' => '1',
        ],[
            'name' => 'Room III',
            'capacity' => '500',
            'cinema_id' => '1',
        ],[
            'name' => 'Room I',
            'capacity' => '150',
            'cinema_id' => '2',
        ],[
            'name' => 'Room II',
            'capacity' => '200',
            'cinema_id' => '2',
        ],[
            'name' => 'Room I',
            'capacity' => '50',
            'cinema_id' => '3',
        ],[
            'name' => 'Room II',
            'capacity' => '250',
            'cinema_id' => '3',
        ],[
            'name' => 'Room III',
            'capacity' => '450',
            'cinema_id' => '3',
        ],[
            'name' => 'Room I',
            'capacity' => '450',
            'cinema_id' => '3',
        ],[
            'name' => 'Room I',
            'capacity' => '200',
            'cinema_id' => '4',
        ],[
            'name' => 'Room II',
            'capacity' => '200',
            'cinema_id' => '4',
        ],[
            'name' => 'Room III',
            'capacity' => '300',
            'cinema_id' => '4',
        ],[
            'name' => 'Room IV',
            'capacity' => '300',
            'cinema_id' => '4',
        ],[
            'name' => 'Room V',
            'capacity' => '500',
            'cinema_id' => '4',
        ],[
            'name' => 'Room I',
            'capacity' => '50',
            'cinema_id' => '5',
        ],[
            'name' => 'Room II',
            'capacity' => '200',
            'cinema_id' => '5',
        ],[
            'name' => 'Room I',
            'capacity' => '1500',
            'cinema_id' => '6',
        ],[
            'name' => 'Room II',
            'capacity' => '1000',
            'cinema_id' => '6',
        ]]);
    }
}
