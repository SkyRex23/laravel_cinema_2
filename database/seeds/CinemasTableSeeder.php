<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([[
            'name' => 'Pathé Balexert',
            'street' => 'Avenue du Pailly 21',
            'postcode' => '1220',
            'city' => 'Geneva',
            'country' => 'Switzerland',
        ],[
            'name' => 'Arena Cinemas',
            'street' => 'Route des Jeunes 10',
            'postcode' => '1227',
            'city' => 'Carouge',
            'country' => 'Switzerland',
        ],[
            'name' => 'Allianz Cinema Genève',
            'street' => 'Quai Gustave-Ador 87',
            'postcode' => '1207',
            'city' => 'Genève',
            'country' => 'Switzerland',
        ],[
            'name' => 'Pathé Archamps',
            'street' => 'Site D',
            'postcode' => '65535',
            'city' => 'Archamps',
            'country' => 'France',
        ],[
            'name' => 'Cinéma Le City',
            'street' => 'Place des Eaux-Vives 3',
            'postcode' => '1207',
            'city' => 'Genève',
            'country' => 'Switzerland',
        ],[
            'name' => 'Cinérama Empire',
            'street' => 'Rue de Carouge 72-74',
            'postcode' => '1205',
            'city' => 'Genève',
            'country' => 'Switzerland',
        ]]);
    }
}
