<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->room ? ',' . $this->room->id : '';

        return [
           'name' => 'required | string | max:50',
           'capacity' => 'required | integer | min: 1| max:65535',
           'cinema_id' => 'required | integer| min: 1 | max:18446744073709551615',
        ];
    }
}
