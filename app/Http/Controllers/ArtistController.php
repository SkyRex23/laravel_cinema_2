<?php

namespace App\Http\Controllers;

use \App\Models\Artist;
use \App\Models\Movie;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * Remplacer all() par paginate($nb) avec nb le nombre d'élément par page
     */
    public function index()
    {
        //$artistmovie = \App\Models\Artist::find($artist_id)->has_played()->get();

        return view('artists.index', ['artists' => Artist::paginate(5)] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\ArtistRequest $request)
    {
        $artist = Artist::create($request->all());

        $photo = $request->file( 'photo' );
        $filename = 'photo_' . $artist->id  . '.' . $photo->guessClientExtension();
        Image::make ( $photo )  ->fit( 180, 240 )
                                -> save( public_path('/uploads/photos/' . $filename ) );

        return redirect()->route('artist.create')
                        ->with('ok',__ ('Artist has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artist = Artist::find($id);
        return view('artists.show')->with('artist', $artist);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        return view('artists.edit', ['artist' => $artist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        $artist->update( $request->all() );

        return redirect()->route('artist.index')
                        ->with('ok', __('Artist has been updated') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        $artist->delete();
        return response()->json();
    }

    public function __construct(){
        $this->middleware('ajax')->only('destroy');
    }
}
