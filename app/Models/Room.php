<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name', 'capacity', 'cinema_id',
    ];

    // Liaison one-to-many avec la table cinema
    public function is_managed_by(){
		return $this->belongsTo(Cinema::class, 'cinema_id');
    }
    
    // Liaison many-to-many avec la table movie 
    // Le pivot permet d'ajouter de nouveaux attributs autres que les clés PK et FK
 	public function projects(){
		return $this->belongsToMany('App\Models\Movie')->withPivot('date_hour_start');
	}
}
