<?php $__env->startSection('title', 'All cinemas'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center;  margin: 50px 0;">All cinemas</h1>

        <?php if(session('ok')): ?>
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                <?php echo e(session('ok')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <?php endif; ?>

        <table class="table table-hover table-striped table-sm">
            <thead>
                <tr>
                    <th scope="row"><?php echo e(__('Name')); ?></th>
                    <th><?php echo e(__('Street')); ?></th>
                    <th><?php echo e(__('Postcode')); ?></th>
                    <th><?php echo e(__('City')); ?></th>
                    <th><?php echo e(__('Country')); ?></th>
                    <th><?php echo e(__('Actions')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $cinemas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cinema): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><a href="/cinema/<?php echo e($cinema->id); ?>"><?php echo e($cinema->name); ?></td>
                    <td><?php echo e($cinema->street); ?></td>
                    <td><?php echo e($cinema->postcode); ?></td>
                    <td><?php echo e($cinema->city); ?></td>
                    <td><?php echo e($cinema->country); ?></td>

                    <td class="table-action">
                        <a type="button" href="<?php echo e(route('cinema.edit', $cinema->id)); ?>" class="btn btn-sm"
                            data-toggle="tooltip" title="<?php echo app('translator')->get('Edit cinema'); ?> <?php echo e($cinema->name); ?>">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                        <a type="button" href="<?php echo e(route('cinema.destroy', $cinema->id)); ?>"
                            class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip"
                            title="<?php echo app('translator')->get('Delete cinema'); ?> <?php echo e($cinema->name); ?>">
                            <i class="fas fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="center-element" style="display: flex; justify-content: center;">
            <?php echo e($cinemas->appends(request()->except('page'))->links()); ?>

        </div>
    </div>
</main>


<script>
    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
        })

        $(document).on('click', '.btn-delete', function(){

            let button = $(this);

            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE'
            }).done(function(){
                button.closest('tr').remove();
            });
            return false;
        });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/cinemas/index.blade.php ENDPATH**/ ?>