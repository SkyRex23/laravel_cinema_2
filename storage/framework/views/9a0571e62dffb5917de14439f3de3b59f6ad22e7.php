<?php $__env->startSection('title', 'Add new cinema'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center;  margin-top: 50px;">Add new movie</h1>

        <form method="POST" action="<?php echo e(route('movie.store')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>


            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" id="year" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="artist_id">Pour quel cinema ?</label>
                <select class="custom-select" name="artist_id" required>
                    <?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>

            <div class="form-group">
                <label for="poster">Poster movie</label>
                <input class="form-control form-control-file form-control-sm" type="file" name="poster" id="poster">
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Create</button>
        </form>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/movies/create.blade.php ENDPATH**/ ?>